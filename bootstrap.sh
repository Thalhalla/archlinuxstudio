#!/bin/bash
cd pacman
sudo cp -av pacman.conf /etc/
sudo cp -av pacman.d/mirrorlist /etc/pacman.d/
sudo pacman -S --noconfirm
sudo pacman -S --noconfirm  powerpill bauerbill
sudo powerpill -Su --noconfirm
sudo powerpill -S --noconfirm base-devel
sudo gpasswd -a $USER audio
sudo gpasswd -a $USER realtime
echo 'vm.swappiness = 10' |sudo tee /etc/sysctl.d/90-swappiness.conf
echo 'fs.inotify.max_user_watches = 600000' |sudo tee /etc/sysctl.d/90-max_user_watches.conf
cd
git clone git://github.com/raboof/realtimeconfigquickscan.git
cd realtimeconfigquickscan
perl ./realTimeConfigQuickScan.pl
